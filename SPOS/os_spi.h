#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "util.h"

#ifndef V4_SPI_H
#define V4_SPI_H
void os_spi_init(void);
uint8_t os_spi_send(uint8_t data);
uint8_t os_spi_receive();
#endif //V4_SPI_H
