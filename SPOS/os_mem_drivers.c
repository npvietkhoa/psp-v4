#include "os_mem_drivers.h"
#include "atmega644constants.h"
#include "os_spi.h"
#include "os_scheduler.h"

//----------------------------------------------------------------------------
// Internal SRAM
//----------------------------------------------------------------------------

//Adresse des Treibers
#define intSRAM (&intSRAM__)


//Treiberfunktionen -- pseudo-function -- nothing to do with
static void initSRAM_internal(void) {}

/*!
 * Read value from address in Medium
 * @param addr address in Medium
 * @return value in given address in Medium
 */
static MemValue readSRAM_internal(MemAddr addr) {
    return *(uint8_t *) addr;
}

/*!
 * Routine that saves value in Medium
 * @param addr The address on Medium the value shall be written to
 * @param value The value to be written
 */
static void writeSRAM_internal(MemAddr addr, MemValue value) {
    *((uint8_t *) addr) = value;
}

// Treiberinstanz
MemDriver intSRAM__ = {
        .init = initSRAM_internal,
        .read = readSRAM_internal,
        .write = writeSRAM_internal,
        .start = AVR_SRAM_START,
        .size = AVR_MEMORY_SRAM
};

//----------------------------------------------------------------------------
// External SRAM
//----------------------------------------------------------------------------

#define EXT_SRAM_START 0
#define EXT_MEMORY_SRAM 65535 //65536 ist die maximale Anzahl an adressierbaren Adressen mit 16 Bit, 65535 ist die größte Zahl, die kleiner als 65536 ist und mit x%3==0 (nötig wegen map und use)

//Hilfsfunktionen
/*!
 * Activates the external SRAM as SPI slave.
 */
void select_memory(void){
    //Set external SRAM as slave
    PORTB &= 0b11101111;
}

/*!
 * Deactivates the external SRAM as SPI slave.
 */
void deselect_memory(void){
    //Deactivate external SRAM as slave
    PORTB |= 0b00010000;
}

/*!
 * Sets the operation mode of the external SRAM
 * @param mode The desired operation mode
 */
void set_operation_mode(uint8_t mode){
    //Set operation mode to mode
    os_enterCriticalSection();
    os_spi_send(mode);
    os_leaveCriticalSection();
}

/*!
 * Transmits a 24bit memory address to the external SRAM.
 * @param addr
 */
void transfer_address(MemAddr addr){
    os_spi_send(0b00000000); //the first seven bits are don't cares, the eighth sent bit ensures that the whole thing is 24 bits long
    if(SPCR & 0b00100000) { // Data Order DORD ist written to 1, the LSB of the data word is transmitted first - 16.5.1 Atmel Document
        os_spi_send(addr); // transfer LSB bits of addr
        os_spi_send(addr>>8); // transfer MSB bits of addr
    } else { // When the DORD bit is written to zero, the MSB of the data word is transmitted first. - 16.5.1 Atmel Document
        os_spi_send(addr>>8); // transfer MSB bits of addr
        os_spi_send(addr); // transfer LSB bits of addr
    }
}

//echte Funktionen
/*!
 * Initialisation of the external SRAM board.
 * This function performs actions such as setting the respective Data Direction Register etc..
 */
static void initSRAM_external(void) {
    os_spi_init();
    //ggf. weitere Steuerleitungen konfigurieren
    select_memory();
    os_spi_send(0x01); //Set operation mode to WRMR - allows to write on MODE register
    set_operation_mode(0x00); // Send 00 (to 6. and 7. Bits of MODE register) to active Byte Operation - 23LC1024 document - 2.5 p.10
    deselect_memory();
}

/*!
 * Private function to read a single byte to the external SRAM It will not check if its call is valid.
 * This has to be done on a higher level.
 * @param addr The address to read the value from
 * @return The read value
 */
static MemValue readSRAM_external(MemAddr addr) {
    MemValue value;
    os_enterCriticalSection();
    select_memory();
    os_spi_send(0x03); // Instruction READ - Reading Mode - 23LC1024 Document - p.06
    transfer_address(addr);
    value = os_spi_receive();
    deselect_memory();
    os_leaveCriticalSection();
    return value;
}

/*!
 * Private function to write a single byte to the external SRAM It will not check if its call is valid.
 * This has to be done on a higher level.
 * @param addr The address to write to
 * @param value The value to be written
 */
static void writeSRAM_external(MemAddr addr, MemValue value) {
    os_enterCriticalSection();
    select_memory();
    os_spi_send(0x02); // Instruction WRITE - Writing Mode - 23LC1024 Document - p.06
    transfer_address(addr);
    os_spi_send(value); //send data to be written
    deselect_memory();
    os_leaveCriticalSection();
}

MemDriver extSRAM__ = {
        .init = initSRAM_external,
        .read = readSRAM_external,
        .write = writeSRAM_external,
        .start = EXT_SRAM_START,
        .size = EXT_MEMORY_SRAM
};

//initialise all memory devices
void initMemoryDevices (void){
    intSRAM__.init();
    extSRAM__.init();
}