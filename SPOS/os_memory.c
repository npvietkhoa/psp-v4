#include "os_memory.h"
#include "os_memory_strategies.h"
#include "util.h"
#include "os_core.h"

//Nibble Manipulation Operations (getters and setters)
void setLowNibble(Heap const *heap, MemAddr addr, MemValue value) {
    uint8_t data = heap->driver->read(addr); // in order to maintain high nibbles if they exists
    data &= 0b11110000; // delete old low nibbles
    //ensure that only the lower part of the value is considered, so that regardless of the value of value only the lower nibble is changed
    value &= 0b00001111;
    data |= value;
    heap->driver->write(addr, data);
}

void setHighNibble(Heap const *heap, MemAddr addr, MemValue value) {
    uint8_t data = heap->driver->read(addr); // in order to maintain low nibbles if they exists
    data &= 0b00001111; // delete old low nibbles
    //ensure that only the higher part of the value is considered, so that regardless of the value of value only the lower nibble is changed
    value = value<<4;
    data |= value;
    heap->driver->write(addr, data);
}

/*!
 *
 * @param heap The heap on whose map the entry is supposed to be set
 * @param addr The address in use space for which the corresponding map entry shall be set
 * @param value The value that is supposed to be set onto the map (valid range: 0x0 - 0xF)
 */
void setMapEntry(Heap const *heap, MemAddr addr, MemValue value) {
    uint16_t addressRange = addr - heap->useStartingAddress;
    bool isOnHighNibble = addressRange % 2 == 0;
    addressRange = addressRange / 2; // cuz each address in map is mapped with 2 in use space
    MemAddr mapAddress = heap->mapStartingAddress + addressRange; // located mapping byte

    if (isOnHighNibble) setHighNibble(heap, mapAddress, value);
    else setLowNibble(heap, mapAddress, value);
}

MemValue getLowNibble(Heap const *heap, MemAddr addr) {
    return (heap->driver->read(addr) & 0b00001111);
}

MemValue getHighNibble(Heap const *heap, MemAddr addr) {
    return ((heap->driver->read(addr) & 0b11110000) >> 4);
}

/*!
 * This function is used to get a heap map entry on a specific heap
 * @param heap The heap from whose map the entry is supposed to be fetched
 * @param addr The address in USE SPACE for which the corresponding map entry shall be fetched
 * @return The value that can be found on the heap map entry that corresponds to the given use space address
 */
MemValue os_getMapEntry(Heap const *heap, MemAddr addr) {
    uint16_t addressRange = addr - heap->useStartingAddress;
    bool isOnHighNibble = addressRange % 2 == 0;
    addressRange = addressRange / 2 ; // cuz each address in map is mapped with 2 in use space
    MemAddr mapAddress = heap->mapStartingAddress + addressRange; // located mapping byte

    if (isOnHighNibble) return getHighNibble(heap, mapAddress);
    return getLowNibble(heap, mapAddress);
}

size_t os_getMapSize(Heap const *heap) {
    return heap->mapSize;
}

size_t os_getUseSize(Heap const *heap) {
    return heap->useSize;
}

MemAddr os_getMapStart(Heap const *heap) {
    return heap->mapStartingAddress;
}

MemAddr os_getUseStart(Heap const *heap) {
    return heap->useStartingAddress;
}

AllocStrategy os_getAllocationStrategy(Heap const *heap) {
    return heap->currentAllocationStrategy;
}

void os_setAllocationStrategy(Heap *heap, AllocStrategy allocStrat) {
    heap->currentAllocationStrategy = allocStrat;
}

//Chunk manipulation operations
uint16_t os_getChunkSizeNonZero(Heap const *heap, MemAddr addr) {
    MemAddr address = os_getFirstByteOfChunk(heap, addr);
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    uint16_t counter = 0;
    if(os_getMapEntry(heap,addr) == 0) {
        while (os_getMapEntry(heap, address) == 0 && address <= useEndAddress) {
            counter++;
            address++;
        }
        return counter;
    }
    else{
        // First Byte in Map-chunk is Nibble, in which pid is saved
        counter++;
        address++;
        while (os_getMapEntry(heap, address) == 0xF && address <= useEndAddress) {
            counter++;
            address++;
        }
        return counter;
    }
}

uint16_t os_getChunkSize(Heap const *heap, MemAddr addr) {
    MemAddr address = os_getFirstByteOfChunk(heap, addr);
    MemAddr useEndAddress = heap->useStartingAddress + heap->useSize - 1;
    uint16_t counter = 0;
    if(os_getMapEntry(heap,addr) == 0) {
        return 0;
    }
    else{
        // First Byte in Map-chunk is Nibble, in which pid is saved
        counter++;
        address++;
        while (os_getMapEntry(heap, address) == 0xF && address <= useEndAddress) {
            counter++;
            address++;
        }
        return counter;
    }
}

/*!
 * This function is used to determine where a chunk starts if a given address might not point to the start of the chunk but to some place inside of it.
 * @param heap The heap the chunk is on hand in
 * @param addr The address that points to some byte of the chunk
 * @return The address that points to the first byte of the chunk
 */
MemAddr os_getFirstByteOfChunk(Heap const *heap, MemAddr addr) {
    MemAddr address = addr;
    if(os_getMapEntry(heap,addr) == 0){
        while (os_getMapEntry(heap, address) == 0 && address >= heap->useStartingAddress) address--;
        address++;
        return address;
    }

    else{
        while (os_getMapEntry(heap, address) == 0xF && address >= heap->useStartingAddress) address--;
        return address;
    }
}

ProcessID getOwnerOfChunk(Heap const *heap, MemAddr addr) {
    MemValue valueOfFirstByte = os_getMapEntry(heap, os_getFirstByteOfChunk(heap, addr));
    switch (valueOfFirstByte) {
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 2;
            break;
        case 3:
            return 3;
            break;
        case 4:
            return 4;
            break;
        case 5:
            return 5;
            break;
        case 6:
            return 6;
            break;
        case 7:
            return 7;
            break;
        default:
            os_errorPStr("First Byte of chunk wrongly identified");
    }
    return 0;
}


/*!
 * Frees a chunk of allocated memory on the medium given by the driver.
 * This function checks if the call has been made by someone with the right to do it
 * (i.e. the process that owns the memory or the OS).
 * This function is made in order to avoid code duplication and is called by several functions that,
 * in some way, free allocated memory such as os_freeProcessMemory/os_free...
 * @param heap The driver to be used.
 * @param addr An address inside of the chunk (not necessarily the start).
 * @param owner The expected owner of the chunk to be freed
 */
void os_freeOwnerRestrictedWithFrames(Heap *heap, MemAddr addr, ProcessID owner) {

    os_enterCriticalSection();

    MemAddr useEndingAddress = heap->useStartingAddress + heap->useSize - 1;

    MemAddr firstByteOfChunk = os_getFirstByteOfChunk(heap, addr); // get chunk first byte so that other method mustn't loop to find it again

    if (owner != getOwnerOfChunk(heap, firstByteOfChunk)) {  // process id will be saved in first byte of chunk
        os_leaveCriticalSection();
        return;
    }

    // get allocFrameStart and allocFrameEnd, which are associative with current heap and process
    MemAddr allocFrameStart = heap->allocFrameStart[owner];
    MemAddr allocFrameEnd = heap->allocFrameEnd[owner];

    bool isAtStartFrame = firstByteOfChunk == heap->allocFrameStart[owner]; // to determine, if we are going to clean first chunk of this process

    MemAddr currAddress = firstByteOfChunk; // for running purpose

    // free first chunk of this owner
    setMapEntry(heap, currAddress, 0); // first Byte of Chunk - pid
    currAddress += 1;
    while (os_getMapEntry(heap, currAddress) == 0xF && currAddress <= allocFrameEnd) {
        setMapEntry(heap, currAddress, 0);
        currAddress += 1;
    }

    currAddress -= 1; // get the last address of this chunk because last step we've added one more

    if (isAtStartFrame && currAddress >= allocFrameEnd) { // this mean every chunk of this owner is cleaned (end is reached)
        // this two variables can be saved with other value but should be equal
        heap->allocFrameStart[owner] = useEndingAddress + 1; // set to biggest and outside useMap
        heap->allocFrameEnd[owner] = heap->useStartingAddress - 1; // set smallest and outside useMap
        os_leaveCriticalSection();
        return; // if all is cleaned then nothing more to be done
    }

    // case we've just cleaned the last chunk of them all, we have to find other endFrame
    // We loop from currAddr - the last addr back until we find the first chunk associates with pid owner
    if (currAddress >= allocFrameEnd) {
        MemAddr lastAddrOfChunk; // last address of the chunk right before which we've cleaned up
        while (currAddress > allocFrameStart && currAddress > heap->useStartingAddress) {
            lastAddrOfChunk = currAddress;
            currAddress = os_getFirstByteOfChunk(heap, currAddress);
            if (getOwnerOfChunk(heap, currAddress) == owner) {
                heap->allocFrameEnd[owner] = lastAddrOfChunk;
                // found! then it's all done
                os_leaveCriticalSection();
                return;
            }
            currAddress -= 1; // jump to next chunk right before
        }
    }

    if(isAtStartFrame) { // if we've just cleaned the first chunk of them all, then we have to find the new startFrame
        while (currAddress < allocFrameEnd && currAddress < useEndingAddress) { // hard condition
            currAddress += 1;
            if (os_getMapEntry(heap, currAddress) == owner) { // found!
                heap->allocFrameStart[owner] = currAddress;
                os_leaveCriticalSection();
                return;
            }
        }
    }

    // leave for last case or if freed Chunk is inside frame
    os_leaveCriticalSection();
}


void os_freeOwnerRestricted(Heap *heap, MemAddr addr, ProcessID owner) {
    if (owner != getOwnerOfChunk(heap, addr)) return;

    MemAddr currAddr = os_getFirstByteOfChunk(heap, addr);
    MemAddr useEndingAddress = heap->useStartingAddress + heap->useSize - 1;

    os_enterCriticalSection();
    setMapEntry(heap, currAddr, 0);
    currAddr++;
    while (os_getMapEntry(heap, currAddr) == 0xF && currAddr <= useEndingAddress) {
        setMapEntry(heap, currAddr, 0);
        currAddr++;
    }
    os_leaveCriticalSection();

}




void os_free(Heap *heap, MemAddr addr) {
    ProcessID currPid = os_getCurrentProc();
    MemAddr endOutBound = heap->useStartingAddress - 1;
    MemAddr startOutBound = heap->useStartingAddress + heap->useSize;
    bool isCreatedWithMalloc = heap->allocFrameStart[currPid] != startOutBound && heap->allocFrameEnd[currPid] != endOutBound;
    if (isCreatedWithMalloc) {
        os_freeOwnerRestrictedWithFrames(heap, addr, currPid);
    } else {
        os_freeOwnerRestricted(heap, addr, currPid);
    }
}

/*!
 * This function realises the garbage collection. When called, every allocated memory chunk of the given process is freed
 * @param heap The heap on which we look for allocated memory
 * @param pid The ProcessID of the process that owns all the memory to be freed
 */
void os_freeProcessMemory(Heap *heap, ProcessID pid) {
    os_enterCriticalSection();
    MemAddr allocFrameStart = heap->allocFrameStart[pid];
    MemAddr allocFrameEnd = heap->allocFrameEnd[pid];
    MemAddr endOutBound = heap->useStartingAddress - 1;
    MemAddr startOutBound = heap->useStartingAddress + heap->useSize;
    while (allocFrameStart !=  startOutBound && allocFrameEnd != endOutBound) { // free until frameStart overlaps / reaches frameEnd
        os_freeOwnerRestrictedWithFrames(heap, allocFrameStart, pid);
        // update frames after each free
        allocFrameStart = heap->allocFrameStart[pid];
        allocFrameEnd = heap->allocFrameEnd[pid];
    }
    os_leaveCriticalSection();
}


MemAddr os_malloc(Heap *heap, uint16_t size) {
    os_enterCriticalSection();

    MemAddr startAddress = 0;
    switch (heap->currentAllocationStrategy) {
        case OS_MEM_FIRST:
            startAddress = os_Memory_FirstFit(heap, size);
            break;

        case OS_MEM_NEXT:
            startAddress = os_Memory_NextFit(heap, size);
            break;

        case OS_MEM_BEST:
            startAddress = os_Memory_BestFit(heap, size);
            break;

        case OS_MEM_WORST:
            startAddress = os_Memory_WorstFit(heap, size);
            break;

        default:
            os_errorPStr("Problems with identifying currAllocStrat");
    }
    if(startAddress == 0) {
        os_leaveCriticalSection();
        return 0;
    }

    setMapEntry(heap,startAddress,os_getCurrentProc()); // first Byte is process id
    for(uint16_t i = 1; i < size; i++){
        setMapEntry(heap,startAddress + i,0xF);
    }


    ProcessID currPid = os_getCurrentProc();
    MemAddr endMallocChunkAddress = startAddress + size - 1;

    // update the frames if possible
    if (startAddress < heap->allocFrameStart[currPid]) heap->allocFrameStart[currPid] = startAddress;
    if (endMallocChunkAddress > heap->allocFrameEnd[currPid]) heap->allocFrameEnd[currPid] = endMallocChunkAddress;

    os_leaveCriticalSection();
    return startAddress;
}

/*!
 * This will move one Chunk to a new location ,
 * To provide this the content of the old one is copied to the new location,
 * as well as all Map Entries are set properly since this is a helper function for reallocation,
 * it only works if the new Chunk is bigger than the old one.
 * @param heap 	The heap where the Moving takes place
 * @param oldChunk The first Address of the old Chunk that is to be moved
 * @param oldSize The size of the old Chunk
 * @param newChunk The first Address of the new Chunk
 * @param newSize The size of the new Chunk
 */
void moveChunk(Heap* heap, MemAddr oldChunk, size_t oldSize, MemAddr newChunk, size_t newSize) {
    if (newSize <= oldSize) return;
    MemValue oldUseData;
    MemAddr volatile currNibble;
    if(newChunk < oldChunk) { // we start copying from the beginning of oldChunk
        for (int i = 0; i < oldSize; i++) {
            // memcpy - copy use-data
            oldUseData = heap->driver->read(oldChunk + i);
            heap->driver->write(newChunk + i, oldUseData);

            // Copy Map-data and unmark old ones
            currNibble = os_getMapEntry(heap, oldChunk + i);
            setMapEntry(heap, newChunk + i, currNibble);
            setMapEntry(heap, oldChunk + i, 0x0);
        }
    } else { // copy from ending of oldChunk
        for (int i = oldSize - 1; i <= 0; i--) {
            // memcpy - copy use-data
            oldUseData = heap->driver->read(oldChunk + i);
            heap->driver->write(newChunk + i, oldUseData);

            // Copy Map-data and unmark old ones
            currNibble = os_getMapEntry(heap, oldChunk + i);
            setMapEntry(heap, newChunk + i, currNibble);
            setMapEntry(heap, oldChunk + i, 0x0);
        }
    }
}

/*!
 * This is an efficient reallocation routine. It is used to resize an existing allocated chunk of memory.
 * If possible, the position of the chunk remains the same.
 * It is only searched for a completely new chunk if everything else does not fit For a more detailed description please use the exercise document.
 * @param heap The heap on which the reallocation is performed
 * @param addr One address inside of the chunk that is supposed to be reallocated
 * @param size The size the new chunk is supposed to have
 * @return First address (in use space) of the newly allocated chunk
 */
MemAddr os_realloc(Heap* heap, MemAddr addr, uint16_t size) {
    os_enterCriticalSection();

    MemAddr useStartingChunkAddress = os_getFirstByteOfChunk(heap, addr);
    uint16_t oldSize = os_getChunkSizeNonZero(heap, useStartingChunkAddress);
    MemAddr useEndingChunkAddress = useStartingChunkAddress + oldSize - 1;

    MemAddr useEndingHeapAddress = heap->useStartingAddress + heap->useSize - 1;

    uint16_t diffChunkSize = size > oldSize ? size - oldSize : oldSize - size; // = abs(size - oldSize)

    if (oldSize >= size) { // in case size will be shrunk or not be changed. In this case useStartingChunkAddress remains the same
        MemAddr currAddress = useEndingChunkAddress;
        while (diffChunkSize > 0) { // Shrink the use data and map data
            setMapEntry(heap, currAddress, 0x0);
            currAddress -= 1;
            diffChunkSize -= 1;
        }
        os_leaveCriticalSection();
        return useStartingChunkAddress;
    }

    // Search on the right side of Chunk
    MemAddr currAddress = useEndingChunkAddress;
    do {
        currAddress += 1;
        if (os_getMapEntry(heap, currAddress) != 0x0 || currAddress > useEndingHeapAddress) { // reach chunk of other process or end of use address
            currAddress -= 1;
            break;
        }
        diffChunkSize -= 1;
    } while (diffChunkSize > 0);

    if(diffChunkSize == 0) { // enough space after chunk (on the right side of Chunk)
        diffChunkSize = size > oldSize ? size - oldSize : oldSize - size; // calculate diffSize again
        for (int i = 1; i <= diffChunkSize; i++) { // Set Nibbles for map - from address right after the last one of chunk
            setMapEntry(heap, useEndingChunkAddress + i, 0xF);
        }
        os_leaveCriticalSection();
        return useStartingChunkAddress; // In this case useStartingChunkAddress remains the same
    }

    // In case there's not enough space after chunk. Search further before the chunk - left of chunk
    // First of all we need to fragment the heap (shift chunk to left to fill free space on left of chunk up)
    currAddress = useStartingChunkAddress;
    size_t freeSpaceBeforeChunk = 0;
    do {
        currAddress -= 1;
        if(currAddress < heap->mapStartingAddress || os_getMapEntry(heap, currAddress) != 0x0) {
            currAddress += 1;
            break;
        }
        freeSpaceBeforeChunk += 1;
    } while (currAddress > heap->mapStartingAddress);

    if (diffChunkSize > freeSpaceBeforeChunk) { // impossible to expand chunk. Search for a new place, which has enough space for new size

        MemAddr newSlotAddr = os_malloc(heap, size);
        if (newSlotAddr == 0) {
            os_leaveCriticalSection();
            return 0; // if malloc is unsuccessful, then return 0 and nothing of current chunk is changed
        }
        moveChunk(heap, useStartingChunkAddress, oldSize, newSlotAddr, size);
        useEndingChunkAddress = newSlotAddr + oldSize - 1;
        diffChunkSize = size > oldSize ? size - oldSize : oldSize - size; // calculate diffSize again
        for (int i = 0; i < diffChunkSize; i++) { // Set rest Nibbles for map to reach desired size - from address right after the last one of chunk
            setMapEntry(heap, useEndingChunkAddress + 1 + i, 0xF);
        }
        os_leaveCriticalSection();
        return newSlotAddr;
    }

    // Chunk must be expanded not only before but also after current Chunk
    // found currAdress is new place where chunk should begin
    moveChunk(heap, useStartingChunkAddress, oldSize, currAddress, size);
    MemAddr newUseEndingChunkAddress = currAddress + oldSize - 1;
    diffChunkSize = size > oldSize ? size - oldSize : oldSize - size; // calculate diffSize again
    for (int i = 1; i <= diffChunkSize; i++) { // Set rest Nibbles for map to reach desired size - from address right after the last one of chunk
        setMapEntry(heap, newUseEndingChunkAddress + i, 0xF);
    }
    os_leaveCriticalSection();
    return currAddress;
}

