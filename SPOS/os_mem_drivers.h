#ifndef V3_OS_MEM_DRIVERS_H
#define V3_OS_MEM_DRIVERS_H

#include <inttypes.h>

//Adresse des Treibers
#ifndef intSRAM
#define intSRAM (&intSRAM__)
#endif

#ifndef extSRAM
#define extSRAM (&extSRAM__)
#endif


typedef uint16_t MemAddr;
typedef uint8_t MemValue;

typedef MemValue MemoryReadHnd(MemAddr addr);

typedef void MemoryWriteHnd(MemAddr addr, MemValue value);

typedef void MemoryInitHnd(void);

struct MemDriver {
    MemoryInitHnd *init;
    MemoryReadHnd *read;
    MemoryWriteHnd *write;
    MemAddr start;
    uint16_t size;
} typedef MemDriver;

MemDriver intSRAM__;

MemDriver extSRAM__;

//! Initialise all memory devices.
void initMemoryDevices(void);

void select_memory(void);
void deselect_memory(void);

#endif //V3_OS_MEM_DRIVERS_H