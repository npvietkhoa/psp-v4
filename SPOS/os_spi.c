#include "os_spi.h"
#include "os_scheduler.h"
#include "os_mem_drivers.h"

/*!
 * Configures relevant I/O registers/pins and initializes the SPI module.
 */
void os_spi_init (void){
    //    PORTB
    //    7. Bit: SCK
    //    6. Bit: SO
    //    5. Bit: SI
    //    4. Bit: ~CS
    // Set SI and SCK as output for the ext. mem.
             //76543210 - index of bits
    PORTB |= 0b10110000;
    PORTB &= 0b11110111; // pull down ~CS to active slave



    // Data Direction and SPI PIN Overrides - Atmel document - p.156
    // Alternative is described in 12.3.2 - p.g 75 - Atmel document
            //76543210 - index of bits
    DDRB |= 0b10110000;
    //Ensure that SO is set as input
    DDRB &= 0b10111111;

    //Activate SPI-Mode and configure AVR as Bus-Master: Set following bits on Control Register (SPCR)
    //    4. Bit (MSTR) set to 1 ~ activate Master
    //    6. Bit (SPE) set to 1 ~ enable SPI
            //76543210 - index of bits
    SPCR |= 0b01010000;


    //Maximize clock frequency: f_cpu / 2
    //    - Control Register (SPCR):
    //        0. Bit (SPRO) set to 0
    //        1. Bit (SPR1) set to 0
    //    - Status Register (SPSR):
    //        0. Bit (SPI2X) set to 1

            //76543210 - index of bits
    SPCR &= 0b11111100;
    SPSR |= 0b00000001;
    // Cation from document: SPIE Bit ~ 7. Bit on Control Register (SPCR) should not be activated. Then set it to 0
            //76543210 - index of bits
    SPCR &= 0b01111111;
}


/*!
 * Performs a SPI send This method blocks until the data exchange is completed.
 * Additionally, this method returns the byte which is received in exchange during the communication.
 * @param data The byte which is send to the slave
 * @return The byte received from the slave
 */
uint8_t os_spi_send (uint8_t data){
    os_enterCriticalSection();
    SPDR = data; // Write data on data register
    while(!(SPSR & 0b10000000)); // Wait for transmission complete ~ SPIF flag gives 1
    uint8_t val = SPDR;
    os_leaveCriticalSection();
    return val;
}

/*!
 * Performs a SPI read This method blocks until the exchange is completed
 * @return The byte received from the slave
 */
uint8_t os_spi_receive (void){
    return os_spi_send(0xFF); // send dummy-byte because transmission is always two-way
}