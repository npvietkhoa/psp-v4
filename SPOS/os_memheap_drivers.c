#include "os_memheap_drivers.h"
#include "defines.h"
#include "os_memory_strategies.h"
#include <avr/pgmspace.h>
#include "os_core.h"

static char const PROGMEM
intStr [] = "internal";

static char const PROGMEM
extStr [] = "external";

#define __heap_start	AVR_SRAM_START + HEAPOFFSET

Heap intHeap__ = {
        .driver = intSRAM,
        .mapStartingAddress = __heap_start,
        .mapSize = ((HEAP_END) - (__heap_start)) / (3),
        .useStartingAddress = (__heap_start) + (((HEAP_END) - (__heap_start)) / (3)),
        .useSize = 2 * (((HEAP_END) - (__heap_start)) / (3)),
        .currentAllocationStrategy = OS_MEM_FIRST,
        .name = intStr
};

void os_initHeaps() {
    for (size_t i = 0; i < os_getHeapListLength(); i++) {
        Heap* currHeap = os_lookupHeap(i);

        // initialize useMap
        for (size_t j = 0; j < (currHeap->mapSize); j++) {
            currHeap->driver->write(currHeap->mapStartingAddress + j, 0);
        }

        // initialize frames
        for (int j = 0; j < MAX_NUMBER_OF_PROCESSES; j++) {
            // ensure startFrame is outside useMap and biggest so that it will always be updated in malloc
            currHeap->allocFrameStart[j] = currHeap->useStartingAddress + currHeap->useSize;
            // analog to frameStart - smallest and outside useMap so that it will always be updated afterward
            currHeap->allocFrameEnd[j] = currHeap->useStartingAddress - 1;
        }
    }
}

size_t os_getHeapListLength(void) {
//    return (sizeof(intHeap) + sizeof(extHeap)) / sizeof(Heap*);
    return 2;
}

Heap* os_lookupHeap(uint8_t index) {
    switch (index){
        case 0: return intHeap;
        case 1: return extHeap;
        default:
            os_errorPStr("Ungueltiger index");
            return (Heap *) 0;
    }
}


#define EXT_SRAM_START 0
#define EXT_MEMORY_SRAM 65535 //would normally be 128kiB, but only half is usable, so 64kiB=65536Bytes, also must be divisible by 3, so 65535Bytes can be used

Heap extHeap__ = {
        .driver = extSRAM,
        .mapStartingAddress = EXT_SRAM_START,
        .mapSize = EXT_MEMORY_SRAM/3,
        .useStartingAddress = EXT_SRAM_START + EXT_MEMORY_SRAM/3,
        .useSize = 2 * (EXT_MEMORY_SRAM/3),
        .currentAllocationStrategy = OS_MEM_FIRST,
        .name = extStr
};