#include "os_scheduling_strategies.h"
#include "defines.h"

#include <stdlib.h>

//----------------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------------

//! Scheduler Information for round robin and inactive aging
SchedulingInformation schedulingInfo;


//----------------------------------------------------------------------------
// Function definitions
//----------------------------------------------------------------------------

/*!
 *  Reset the scheduling information for a specific strategy
 *  This is only relevant for RoundRobin and InactiveAging
 *  and is done when the strategy is changed through os_setSchedulingStrategy
 *
 * \param strategy  The strategy to reset information for
 */
void os_resetSchedulingInformation(SchedulingStrategy strategy) {
    // This is a presence task
    if (strategy == OS_SS_ROUND_ROBIN) {
        schedulingInfo.timeSlice = os_getProcessSlot(
                os_getCurrentProc())->priority; // for round-robin timeSlice = priority
        return; // terminate this method immediately
    }

    if (strategy == OS_SS_INACTIVE_AGING) {
        for (uint8_t i = 0; i < MAX_NUMBER_OF_PROCESSES; ++i) {
            schedulingInfo.age[i] = 0; // with inactive aging set all the routine of age to 0
        }
    }
}

/*!
 *  Reset the scheduling information for a specific process slot
 *  This is necessary when a new process is started to clear out any
 *  leftover data from a process that previously occupied that slot
 *
 *  \param id  The process slot to erase state for
 */
void os_resetProcessSchedulingInformation(ProcessID id) {
    // This is a presence task
    schedulingInfo.age[id] = 0;
}

/*!
 *  This function implements the even strategy. Every process gets the same
 *  amount of processing time and is rescheduled after each scheduler call
 *  if there are other processes running other than the idle process.
 *  The idle process is executed if no other process is ready for execution
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the even strategy.
 */
ProcessID os_Scheduler_Even(Process const processes[], ProcessID current) {
    //#warning IMPLEMENT STH. HERE
    ProcessID procId = current;
    do {
        procId += 1;
        if (procId >= MAX_NUMBER_OF_PROCESSES && current == 0) return 0;
        if (procId >= MAX_NUMBER_OF_PROCESSES) procId = 1;
        if (processes[procId].state == OS_PS_READY) return procId;
    } while (procId != current);

    return 0;
}

/*!
 *  This function implements the random strategy. The next process is chosen based on
 *  the result of a pseudo random number generator.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the random strategy.
 */
ProcessID os_Scheduler_Random(Process const processes[], ProcessID current) {
    //#warning IMPLEMENT STH. HERE

    // Get the number of processes, which are currently ready to run
    uint8_t numberOfActiveProcs = 0;
    ProcessID procId = 1;
    do {
        numberOfActiveProcs += os_getProcessSlot(procId)->state == OS_PS_READY;
        procId += 1;
    } while (procId < MAX_NUMBER_OF_PROCESSES);

    if (numberOfActiveProcs == 0) return 0; // if there's no active process at all then start empty process

    int randomNumber = rand() % numberOfActiveProcs; // generate random number for picking up the next process

    procId = 0;
    do {
        procId += 1;
        if (procId >= MAX_NUMBER_OF_PROCESSES) return 0;
        if (processes[procId].state == OS_PS_READY) randomNumber -= 1;
    } while (randomNumber >= 0);

    return procId;
}

/*!
 *  This function implements the round-robin strategy. In this strategy, process priorities
 *  are considered when choosing the next process. A process stays active as long its time slice
 *  does not reach zero. This time slice is initialized with the priority of each specific process
 *  and decremented each time this function is called. If the time slice reaches zero, the even
 *  strategy is used to determine the next process to run.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed determined on the basis of the round robin strategy.
 */
ProcessID os_Scheduler_RoundRobin(Process const processes[], ProcessID current) {
    // This is a presence task
    if (schedulingInfo.timeSlice > 1 && processes[current].state == OS_PS_READY) {
        schedulingInfo.timeSlice -= 1;
        return current;
    }

    ProcessID nextPid = os_Scheduler_Even(processes, current);
    schedulingInfo.timeSlice = processes[nextPid].priority;
    return nextPid;
}

/*!
 *  This function realizes the inactive-aging strategy. In this strategy a process specific integer ("the age") is used to determine
 *  which process will be chosen. At first, the age of every waiting process is increased by its priority. After that the oldest
 *  process is chosen. If the oldest process is not distinct, the one with the highest priority is chosen. If this is not distinct
 *  as well, the one with the lower ProcessID is chosen. Before actually returning the ProcessID, the age of the process who
 *  is to be returned is reset to its priority.
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed, determined based on the inactive-aging strategy.
 */
ProcessID os_Scheduler_InactiveAging(Process const processes[], ProcessID current) {
    // This is a presence task
    Age maxAge = 0;
    for (ProcessID pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY) {
            schedulingInfo.age[pid] += processes[pid].priority; // Update ages for processes

            // find the max age simultaneously
            if (schedulingInfo.age[pid] > maxAge) {
                maxAge = schedulingInfo.age[pid];
            }
        }
    }

    // Loop once to find first proc with maxAge - cuz possibly more than one has the same age and priority
    ProcessID lowestPIDProc = 0;
    ProcessID highestPriorityProc = 0;

    for (ProcessID pid = 1; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY && schedulingInfo.age[pid] == maxAge) {
            lowestPIDProc = pid;
            highestPriorityProc = pid;
            break;
        }
    }

    // Loop forward all the process array to find the process, which allowed performing next
    for (ProcessID pid = lowestPIDProc; pid < MAX_NUMBER_OF_PROCESSES; pid++) {
        if (processes[pid].state == OS_PS_READY &&
            processes[pid].priority > processes[highestPriorityProc].priority &&
            schedulingInfo.age[pid] == maxAge) {
            highestPriorityProc = pid;
        }
    }

    if (lowestPIDProc == highestPriorityProc) { // Just one process found or processes have the same priority and age
        schedulingInfo.age[lowestPIDProc] = 0; // set age back to 0
        return lowestPIDProc;
    } else {    // If there's more than one with same maxAge, then choose one which the highest priority
        schedulingInfo.age[highestPriorityProc] = 0; // set age back to 0
        return highestPriorityProc;
    }
}

/*!
 *  This function realizes the run-to-completion strategy.
 *  As long as the process that has run before is still ready, it is returned again.
 *  If  it is not ready, the even strategy is used to determine the process to be returned
 *
 *  \param processes An array holding the processes to choose the next process from.
 *  \param current The id of the current process.
 *  \return The next process to be executed, determined based on the run-to-completion strategy.
 */
ProcessID os_Scheduler_RunToCompletion(Process const processes[], ProcessID current) {
    // This is a presence task
    if (processes[current].state == OS_PS_READY) return current;
    return os_Scheduler_Even(processes, current);
}
